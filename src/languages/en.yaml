ARCHMAGE:
  abilities: Abilities
  abilityName: Ability Name
  act: Act.
  action: Action
  actions: Actions
  add: Add
  at-will: At-Will
  attack: Attack
  attackMod: Attack Modifier
  attributes: Attributes
  avatarAlt: Character profile image
  backgrounds: Backgrounds
  baseAC: Base AC
  baseHP: Base HP
  baseMD: Base MD
  basePD: Base PD
  baseRecoveries: Base Recoveries
  biography: Biography
  bonus: Bonus
  bonuses: Bonuses
  byAction: Group by if the Power has an Action or Not
  byGroup: Group by manual Power Group value entry
  byType: Group by Power Type
  calculateHP: Calculate Max HP?
  calculateRecoveries: Calculate Max Recoveries?
  chakra: Chakra
  class: Class
  conflicted: Conflicted
  create: Create
  critModAtk: Crit Modifier (Attack)
  critModDef: Crit Modifier (Defense)
  daily: Daily
  defenses: Defenses
  description: Description
  details: Details
  disengageAdjustment: Disengage Adjustment
  edit: Edit
  effects: Effects
  equipment: Equipment
  equipmentName: Equipment Name
  feat: Feat
  feats: Feats
  feature: Feature
  features: Features
  filter: Filter
  filterName: Filter by name
  flavor: Flavor
  flexible: Flexible Attack
  flexibles: Flexible Attacks
  free: Free Action
  groupBy: Group by
  health: Health
  hit: Hit
  hitPoints: Hit Points
  iconRelationships: Icon Relationships
  import: Import Powers
  incrementalAdvances: Incremental Advances
  incrementals: Incrementals
  initAdjustment: Initiative Adjustment
  initiative: Initiative
  interrupt: Interrupt Action
  inventory: Inventory
  item: 'Item'
  jabWeaponDice: JAB Weapon Dice
  keyMod: Key Modifier
  keyReduced: Reduced to {mod} due to {kmod1}/{kmod2} key modifier
  kickWeaponDice: KICK Weapon Dice
  level: Level
  loot: Loot
  lvl: Lvl
  magicItems: Magic Items
  maneuver: Maneuver
  maneuvers: Maneuvers
  melee: Melee
  meleeAbility: Melee Ability
  meleeDamageAbility: Melee Damage Ability
  meleeMiss: Melee Miss Dmg.
  meleeWeaponDice: Melee Weapon Dice
  misc: Misc.
  miscellaneous: Miscellaneous
  miss: Miss
  mod: Mod
  modifyLevel: Modify Level
  move: Move Action
  name: Name
  nastierSpecial: Nastier Special
  nastierSpecials: Nastier Specials
  negative: Negative
  once-per-battle: Per Battle
  oneUniqueThing: One Unique Thing
  other: Other
  others: Other
  positive: Positive
  power: Power
  powerName: Power Name
  powers: Powers
  punchWeaponDice: PUNCH Weapon Dice
  quantity: Quantity
  quick: Quick Action
  race: Race
  ranged: Ranged
  rangedAbility: Ranged Ability
  rangedDamageAbility: Ranged Damage Ability
  rangedMiss: Ranged Miss Dmg.
  rangedWeaponDice: Ranged Weapon Dice
  rchg: Rchg.
  recharge: Recharge
  recoveries: Recoveries
  recoveryDice: Recovery Dice
  resistance: Resistance
  role: Role
  saves: Saves
  size: Size
  sort: Sort
  specialTraits: Special Traits
  spell: Spell
  spells: Spells
  standard: Standard Action
  talent: Talent
  talents: Talents
  tempHp: Temp HP
  tool: Tool
  trait: Trait
  traits: Traits
  type: Type
  uses: Uses
  vulnerability: Vulnerability
  # Settings
  SETTINGS:
    automateBaseStatsFromClassHint: Automatically populate AC, PD, MD, base HP, Recovery and Weapon dices based on class name(s).
    automateBaseStatsFromClassName: Automatically compute base stats
    automateHPConditionsHint: If enabled, the Staggered and Dead conditions will be automatically applied and removed after hp changes; Disable this if you are using custom conditions via modules such as Combat Utility Belt - it is implicitly disabled if that module is active.
    automateHPConditionsName: Auto-manage Dead and Staggered
    automatePowerCostHint: Remove resources (e.g. Command Points, Ki or Focus) automatically when using a power with a cost?
    automatePowerCostName: Automatically apply power costs
    ColorblindHint: Change the colors of the power cards to more accessibility color palette.
    ColorblindName: Enable Colorblind Accessibility Mode
    ColorblindOptioncolorBlindBY: Tritanopia (Blue/Yellow)
    ColorblindOptioncolorBlindRG: Protanopla / Deutanopla (Red/Green)
    ColorblindOptionDefault: Default Colors
    extendedStatusEffectsHint: If enabled extends the official status list with a handful of useful token markers.
    extendedStatusEffectsName: Extended Status Effects List
    hideExtraRollsHint: If enabled, powers with multiple attack rolls will display at most one attack per targeted token if any is targeted.
    hideExtraRollsName: Limit extra rolls to targeted tokens
    hideInsteadOfOpaqueHint: Enable this if you prefer not seeing inactive details at all.
    hideInsteadOfOpaqueName: Hide inactive Features / Triggers instead of making them faded-out
    initiativeDexTiebreakerHint: If enabled initiative ties will be resolved using the initiative modifier; if not, monsters win by default.
    initiativeDexTiebreakerName: Initiative Modifier as Tiebreaker
    multiTargetAttackRollsHint: Enable this to automatically duplicate inline attack rolls for multi-target powers based on the number of targets.
    multiTargetAttackRollsName: Multi-Target Attack Rolls
    rechargeOncePerDayHint: Attempt to recharge powers on quick rests only once (true) or re-attempt after every quick rest (false)?
    rechargeOncePerDayName: Recharge once per day only
    RoundUpDamageApplicationHint: This will be applied the same way for both characters and npcs
    RoundUpDamageApplicationName: When applying damage via the right-click context menu, should it round up (true) or round down (false)?
    ShowDebugLogsHint: Turn this on for bug reports
    ShowDebugLogsName: Show debug logs?
    showDefensesInChatHint: Enable this display a list of targeted defenses in the attack line of chat power cards.
    showDefensesInChatName: Show target(s) defense(s) in chat
    staggeredOverlayHint: If enabled the staggered condition will be automatically applied as a token overlay instead of a normal condition.
    staggeredOverlayName: Staggered as overlay
    tourVisibilityAll: Everyone can see tours
    tourVisibilityGM: Only the GM can see tours
    tourVisibilityHint: When new versions of Toolkit13 are released, the system will display a tour of the changes. Use this setting to determine who should see the tours (or if they should be disabled).
    tourVisibilityName: Features tour visibility
    tourVisibilityOff: Tours are disabled
    UnboundEscDieHint: Enable this setting to let the escalation die go above 6 and below 0 when manually adjusted (still remains limited when counting by the current round).
    UnboundEscDieName: Unlimited Escalation Die
  AUTOLEVEL:
    confirm: Confirm (create duplicate)
    help: Use this form to create a copy of this actor with its stats (including attacks) adjusted to the new level.
    newLevel: New Level
  # Characters
  CHARACTER:
    RESOURCES:
      commandPoints: Command Points
      cp: Cmd
      custom1: Custom Resource 1
      custom2: Custom Resource 2
      custom3: Custom Resource 3
      custom4: Custom Resource 4
      custom5: Custom Resource 5
      custom6: Custom Resource 6
      custom7: Custom Resource 7
      custom8: Custom Resource 8
      custom9: Custom Resource 9
      focus: Focus
      ki: Ki
      momentum: Momentum
  CHARACTERFLAGS:
    improvedIniativeHint: General feat to increase initiative by +4.
    improvedIniativeName: Improved Initiative
    initiativeAdvHint: Human racial feat to roll 2d20 for initiative and keep the higher roll.
    initiativeAdvName: Quick to Fight
    title: Flags
  CHARACTERSETTINGS:
    bg1: Background 1
    bg2: Background 2
    bg3: Background 3
    bg4: Background 4
    bg5: Background 5
    bg6: Background 6
    bg7: Background 7
    bg8: Background 8
    dualwield: Dual-Wielding
    i1: Icon 1
    i2: Icon 2
    i3: Icon 3
    i4: Icon 4
    i5: Icon 5
    settings: Settings
    shield: Shield
    twohanded: Two-handed Weapon
  INCREMENTALS:
    abilityScoreBonusHint: 4th/7th/10th Level (+1 to 3 abilities)
    abilityScoreBonusName: Ability Score Bonus
    extraMagicItemHint: Carry an extra Magic Item without becoming overwhelmed by Quirks
    extraMagicItemName: Extra Magic Item
    featHint: Choose one feat as if you were the next level
    featName: Feat
    featureHint: Choose one class feature as if you were the next level
    featureName: Feature
    hpHint: Increase your HP as if you were the next level
    hpName: Hit Points
    iconRelationshipPointHint: 5th/8th Level
    iconRelationshipPointName: Icon Relationship Point
    powerSpell1Hint: Choose a power / spell as if you were the next level
    powerSpell1Name: Power / Spell
    powerSpell2Hint: Choose a power / spell as if you were the next level
    powerSpell2Name: Power / Spell
    powerSpell3Hint: Choose a power / spell as if you were the next level
    powerSpell3Name: Power / Spell
    powerSpell4Hint: Choose a power / spell as if you were the next level
    powerSpell4Name: Power / Spell
    powerSpellHint: Choose a power / spell as if you were the next level
    powerSpellName: Power / Spell
    skillsHint: Add +1 to all skill rolls
    skillsName: Skills
    talentHint: Choose one talent as if you were the next level
    talentName: Talent
  COINS:
    copper: Copper
    copperHint: 10 Copper = 1 Silver
    gold: Gold
    goldHint: 1 Gold = 10 Silver
    platinum: Platinum
    platinumHint: 1 Platinum = 10 Gold
    silver: Silver
    silverHint: 1 Silver = 10 Copper
  PREPOPULATE:
    help: "Click the checkboxes to the left of any powers you would like to add, and then click submit to add them to your character. You can click any power's name to expand it and view its text and available feats."
# Ability Scores
  str:
    key: STR
    label: Strength
  dex:
    key: DEX
    label: Dexterity
  con:
    key: CON
    label: Constitution
  int:
    key: INT
    label: Intelligence
  wis:
    key: WIS
    label: Wisdom
  cha:
    key: CHA
    label: Charisma
  # Defenses
  ac:
    key: AC
    label: Armor Class
    stats: Con/Dex/Wis
  pd:
    key: PD
    label: Physical Defense
    stats: Str/Con/Dex
  md:
    key: MD
    label: Mental Defense
    stats: Int/Wis/Cha
  GROUPS:
    actionType: Action
    group: Custom Groups
    powerSource: Class/Race/Item
    powerType: Type
    powerUsage: Usage
  MONKFORMS:
    aelabel: Monk AC bonus
    finishing: finishing
    flow: flow
    opening: opening
  RESTS:
    full: Refill on Full Heal-Up
    fullreset: Clear on Full Heal-Up
    header: On resting
    none: Do nothing
    quick: Refill on Quick Rest
    quickreset: Clear on Quick Rest
  SORTS:
    custom: Custom
    level: Level
    name: Name
  INVENTORY:
    equipment: Magic Items
    loot: Loot
    tool: Tools
  ITEM:
    acBonus: AC Bonus
    active: Active
    arcaneAttackBonus: Arcane Spell / Arcane Attack Bonus
    arcaneBonus: Arcane Spell / Arcane Attack Bonus
    attackBonuses: Attack Bonuses
    chakraSlot: Chakra Slot
    defenseBonuses: Defense Bonuses
    disengageBonus: Disengage Bonus
    divineAttackBonus: Divine Spell / Divine Attack Bonus
    divineBonus: Divine Spell / Divine Attack Bonus
    group: Things with the same group will be grouped together
    hpBonus: HP Bonus
    mdBonus: MD Bonus
    meleeAttackBonus: Melee Attack Bonus
    meleeBonus: Melee Attack Bonus
    pdBonus: PD Bonus
    price: Price
    rangedAttackBonus: Ranged Attack Bonus
    rangedBonus: Ranged Attack Bonus
    recoveriesBonus: Recoveries Bonus
    saveBonus: Save Bonus
  CHAKRA:
    armor: Armor, robe, shirt, tunic
    arrow: Arrow, crossbow bolt, slingstone
    belt: Belt, swordbelt, kilt, girdle
    book: Book, scroll, manual, grimoire
    boots: Boots, scandals, slippers, shoes
    cloak: Cloak, mantle, cape
    glove: Glove, gauntlet, mitt
    helmet: Helmet, circlet, crown, cap
    melee: Weapon, melee
    necklace: Necklace, pendant
    none: None
    ranged: Weapon, ranged
    ring: Ring
    shield: Shield
    staff: Staff (implement)
    symbol: Symbol, holy relic, sacred sickle (implement)
    wand: Wand (implement)
    wondrous: Wondrous item
  CHAT:
    actionType: Action Type
    adventurer: Adventurer Feat
    adventurerDescription: Adventurer Feat Description
    always: Always
    attack: Attack
    breathWeapon: Breath Weapon
    Cancel: Cancel
    castBroadEffect: Cast for Broad Effect
    castPower: Cast for Power
    champion: Champion Feat
    championDescription: Champion Feat Description
    CmdPtsReset: reset to 1
    complicatedAdvantages: Complicated Advantages
    cost: Cost
    crit: Crit
    Delete: Delete
    DeleteConfirm: Are you sure you want to delete this?
    effect: Effect
    epic: Epic Feat
    epicDescription: Epic Feat Description
    finalVerse: Final Verse
    FullHeal: Full Heal-Up
    FullHealBody: Regain all health, recoveries, and power uses.
    FullHealHP: reset to maximum
    group: Group
    hit: Hit
    hit1: Special Hit (1)
    hit2: Special Hit (2)
    hit3: Special Hit (3)
    hitEven: Even Hit
    hitOdd: Odd Hit
    HP: HP
    ItemReset: restored uses to
    KiReset: reset to
    embeddedMacro: Embedded Macro
    embeddedMacroSummary: Edit the embedded macro that will run when this power is used.
    embeddedMacroPermissionError: Cannot execute embedded macro - request permission for this user to your GM!
    miss: Miss
    missEven: Even Miss
    missOdd: Odd Miss
    MomentReset: lost
    NoResources: Not enough resources
    NoUses: No uses left
    NoUsesMsg: No uses left, are you sure you want to use this?
    powerLevel: Level
    powerSource: Power Source
    powerSourceName: Source Name
    powerOriginName: Original Name
    powerType: Power Type
    powerUsage: Power Usage
    QuickRest: Quick Rest
    QuickRestBody: Regain per-battle powers, roll to recharge spent recharge powers, and roll recoveries to heal above 50% health.
    QuickRestHP1: rolled
    QuickRestHP2: recoveries healing
    range: Range
    recharge: Recharge
    RechargeFail: recharge failed
    RechargeSucc: recharged
    Rest: Rest
    Rests: Rests
    sequencerTarget: Sequencer File (Target)
    sequencerRay: Sequencer File (Ray)
    sequencerSelf: Sequencer File (Self)
    sequencerReverse: Reverse Sequence
    special: Special
    spellChain: Chain Spell
    spellLevel3: 3rd Level Spell
    spellLevel5: 5th Level Spell
    spellLevel7: 7th Level Spell
    spellLevel9: 9th Level Spell
    rollTable: RollTable
    sustainedEffect: Opening and Sustained Effect
    sustainOn: Sustain On
    target: Target
    trigger: Trigger
    unambiguousAdvantages: Unambiguous Advantages
    Use: Use
  TARGETING:
    '2': two
    '3': three
    '4': four
    '5': five
    '6': six
    '7': seven
    '8': eight
    '9': nine
    all: all
    crescendoSpecial: You can choose more than one target for this spell, but you take a –2 penalty when attacking two targets, a –3 penalty for three targets, and so on.
    each: each
    every: every
    random: random
  SAVE:
    death: Death Save
    disengage: Disengage
    easy: Easy Save
    easyShort: Easy
    failure: Failed
    hard: Hard Save
    hardShort: Hard
    lastGasp: Last Gasp Save
    normal: Normal Save
    normalShort: Normal
    success: Succeeded
  EFFECT:
    StatusAsleep: Asleep
    StatusBlessed: Blessed
    StatusBonusDefenses: Bonus Defenses
    StatusBuffed: Buffed
    StatusConfused: Confused
    StatusCursed: Cursed
    StatusDazed: Dazed
    StatusDead: Dead
    StatusDebuffed: Debuffed
    StatusEmpowered: Empowered
    StatusFear: Fear
    StatusFireShield: Fire Shield
    StatusFlying: Flying
    StatusGrabbed: Grabbed
    StatusHampered: Hampered
    StatusHelpless: Helpless
    StatusHidden: Hidden
    StatusHolyShield: Holy Shield
    StatusIceShield: Ice Shield
    StatusLastGasps: Last Gasps
    StatusMageShield: Mage Shield
    StatusOngoingDamage: Ongoing Damage
    StatusReducedDefenses: Reduced Defenses
    StatusRegen: Regeneration
    StatusShining: Radiating Light
    StatusShocked: Shocked
    StatusStaggered: Staggered
    StatusStuck: Stuck
    StatusStunned: Stunned
    StatusUnconscious: Unconscious
    StatusVulnerable: Vulnerable
    StatusWeakened: Weakened
  UI:
    errNotEnoughCP: You don't have enough Command Points to use this command. Use anyway?
    errNotEnoughKi: You don't have enough Ki to use this power. Use anyway?
    errNoMomentum: You need Momentum to use this power. Use anyway?
    errNoFocus: You need Focus to use this power. Use anyway?
    errNoCustomResource: You don't have enough
    errNoCustomResource2: Use anyway?
    errNotInteger: must be an integer number
    errDependency: The <strong>vueport</strong> and <strong>dlopen</strong> modules are required for the V2 character sheet. Return to the Foundry setup page and use the module installer to install both of those modules to allow the V2 character sheet to be available.
    enableDependencies: Enable dependencies?
    dependencyContent: The V2 character sheet requires the VuePort and dlopen modules to be enabled. Would you like to enable them now?
    noToken: No token selected.
    tooManyLevels: Changing a monster by more than 6 levels is not recommended.
    levelLimits: Level can't increase above 15 or decrease below 0.
    classChange: Class change detected, base stats adjusted.
  MIGRATIONS:
    start: 'Applying Toolkit13 System Migration version {version}. Please be patient and do not close your game or shut down your server.'
    complete: "Toolkit13 System Migration to version {version} completed!"
    updateActors: 'Updating world actors'
    updateTokens: 'Updating world tokens'
    updateCompendiumActors: 'Updating compendium actors: {pack}'
    updateCompendiumScenes: 'Updating compendium scenes: {pack}'
    '1_19_0': "Toolkit13 1.19.0: Updating NPC initiative to use a flat number and not calculate it based on level."
# Tours
  TOURS:
    '170':
      welcome: Welcome to Toolkit13 (13th Age Compatible) version 1.7.0! Here's a quick tour of new features
      documentation: We have started the (long) process of writing documentation for the system. If you have documentation you want to contribute, please reach out or issue a Pull Request!
      cards: We now have Card Support for Icon Relationship 5's and 6's - Please see the documentation for more info on how to enable and set this up
      sheets: Character sheets have been reworked. Open one up to check it out!
      showItem: You can now show an Item to all players with a new 'Show to Players' button
      iconRelationships: Rolling Icon Relationships now has a nicer Chat display, along with Disengage and Save checks
      monstersCompendium: There is now a system-provided Monsters Compendium
      generalFeats: Along with General Feats, courtesy of the Community!
      end: "There's a variety of bug fixes and minor tweaks as well, such as to Trigger automation. Come join us in the Foundry Discord #13th-age channel for questions and feedback!"
    '180':
      welcome: Welcome to Toolkit13 (13th Age Compatible) version 1.8.0! Here's a quick tour of new features
      monkCompendium: The Monk compendium is back up and available to use! Monk forms come preloaded with groups, so checkout the manual group sorting option on monk character sheets! Go jab, punch, and kick your way to victory!
      escalationDie: The Escalation die is now manually adjustable, and via a new System Setting can even go past 9000! (6 is the default max)
      tierShorthand: Thanks to LegoFed3, there is now a @tier shorthand for Actors. Tier is the multiplier for each tier (1x adv, 2x cha, 3x epic)
      end: "There's a variety of bug fixes and minor tweaks as well, such as multi-class health calculations and various compendium updates. Come join us in the Foundry Discord #13th-age channel for questions and feedback!"
    '190':
      welcome: Welcome to Toolkit13 (13th Age Compatible) version 1.9.0! Here's a quick tour of new features
      aip: Added basic support for the Autocomplete Inline Properties (https://foundryvtt.com/packages/autocomplete-inline-properties/) module, if enabled.
      necromancer: The Necromancer compendium is back up and available to use! Go Raise Hell!
      colorblind: Thanks to EndlesNights, we now have colorblind options. Check out the System Settings - powers should be much clearer for colorblind players!
      baseStats: HP, AC, Damage dice, and more are now optionally calculated automatically based on your Class. Huge thanks to LegoFed3 for this!
      inlineRoll: All Power Compendiums should now have inline Rolls. Thanks to everyone in the community who helped!
      inlineRollDocs: We now have additional documentation for inline Rolls in the Reference dialog!
      systemDocs: Our System documentation has been expanded even further, and is now in Gitbooks for a better experience
      chatCards: Ability / Background checks now have a new Chat Card layout that includes useful information such as "Background used". The Icon Roll Chat Cards got a makeover as well - roll the dice and see what the Icons bring!
      end: "There's a variety of bug fixes and minor tweaks as well, such as fixing powers in the Compendiums, adding Weakling / Elite to creature sizes, negative recovery logic, and more. Come join us in the Foundry Discord #13th-age channel for questions and feedback!"
    '1100':
      welcome: Welcome to Toolkit13 (13th Age Compatible) version 1.10.0! Here's a quick tour of new features
      v2Sheet: We've redesigned the character sheet! The overall structure is the same, but everything that's rollable will now have a blue d20 near it, and the sheet will feel more responsive. If you prefer the original sheet, you can return to the original sheet per character by clicking the "Sheet" settings gear in the header of the sheet window.
      nightMode: The new character sheet has a night mode! Click the <i class="fas fa-cogs"></i> tab on the sheet and find it near the bottom of the right sidebar.
      rolls: Many more areas of the sheet are now rollable, such as initiative, death saves, icon rolls, and more. Watch out for a blue <i class="fas fa-dice-d20"></i> icon to find rollable links!
      iconRolls: Icon rolls are now stored on the sheet! You can also click an icon result icon to cycle through 5, 6, and empty results.
      rests: The sheet now supports both quick rests and full heal-ups! Find them in the new resources section of the character sheet directly above powers.
      powerImporter: The power importer has been redesigned! The new design is much closer to a character builder, and will pull from any classes and races detected on your character sheet.
      end: "There's also a variety of bug fixes and minor tweaks added to the system! Come join us in the Foundry Discord #13th-age channel for questions and feedback!"
    '1170':
      welcome: Welcome to Toolkit13 (13th Age Compatible) version 1.17.0! We have some important changes for critical hits in this release.
      crits: <strong>Crits no longer double damage automatically.</strong>. Instead, each target that was critted against will be <strong>bold</strong> in the chat card. We've also added offensive and defensive crit modifiers as new settings within character sheets, and we've added an Active Effect to the vulnerable condition.
      compendia: We have several corrections to existing compendia entries, and we've added <strong>magic items</strong> from the SRD!
      end: "There's also a variety of bug fixes and minor tweaks added to the system! You can read the full release notes <a href='https://gitlab.com/asacolips-projects/foundry-mods/archmage/-/releases/1.17.0' target='_blank'>here</a>. Come join us in the Foundry Discord #13th-age channel for questions and feedback!"
    '1190':
      welcome: Welcom to Toolkit13 (13th Age Compatible) version 1.19.0! We have some big changes for NPCs including a new sheet and updated compendiums!
      end: "You can read the full release notes <a href='https://gitlab.com/asacolips-projects/foundry-mods/archmage/-/releases/1.19.0' target='_blank'>here</a>. Come join us in the Foundry Discord #13th-age channel for questions and feedback!"
      npc:
        welcome: We've updated the NPC sheet to a new design! The original NPC sheet is still available and can be switched to by setting the sheet to the "Legacy NPC Sheet".
        init: NPC initiative no longer includes the NPC's level. Now, NPC initiative will use the exact value you enter, and we've included a migration to update all of your actors, tokens, and compendiums to account for it.
        header: The header now includes flavor text and a cleaner presentation for its fields. Some fields in the header have to be clicked to edit them.
        attributes: Attributes are now in a row directly below the header and above the NPC sheet tabs. These include defenses and hit points as before, but now there are two new columns for Saves and Disengage checks that work the same as they do on the PC character sheet.
        compact: There's also a small arrow in the bottom right corner of the NPC sheet header (just below the NPC's avatar) that puts the sheet into a compact mode by reducing the size of the header and attributes and hiding some items less important for play such as flavor text. This is handled as a flag per actor, so it will persist between page reloads.
        details: The NPC's biography! This field always existed, but we never had a place for it before. We're likely going to expand this section to include other long text fields like battle tactics, story hooks, and icons.
        actions: |
          <div>Actions, Traits, and Nastier Specials now work similar to how Powers work on character sheets.</div>
          <ul>
            <li>All three types support avatars (including chat)!</li>
            <li>Actions are summarized and can be expanded on click.</li>
            <li>Inline rolls display in a new summarized format, such as <strong style="color:#671212;">+8+LVL+ED</strong>.</li>
          </ul>
        effects: These work exactly like they do for character sheets! Conditions and custom effects will show up here and can be created, edited, or removed right from the sheet.
        modifyLevel: |
          <p>The modify level tab includes a slider that lets you adjust the level of the monster within the constraints allowed by the core rules (+/- 6, min 0, max 15). Clicking confirm will create a copy of the monster, and the new monster will appear in your world's actor directory. The system will make a best effort calculation to change the NPC's stats, attacks, and damage.</p>
          <p>This feature is experimental, and we're looking forward to improving it with feedback on what works and what doesn't!</p>
        settings: As with the character sheet, settings have been moved into their own tab. This currently only includes visual information such as the avatar settings and the ability to switch the NPC sheet to night mode, but we'll likely expand this in the future with additional settings!
      compendiums: |
        <strong>SRD Monster Compendiums</strong> have been updated to include inline rolls for <em>most</em> use cases. There have been a few outliers that slipped through the cracks, but virtually every monster in the SRD compendium was updated to have inline rolls added for their targeting lines and their damage. This only applies to monsters in the compendium.